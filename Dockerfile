FROM node:14.9-alpine as build

WORKDIR /usr/share/api-gateway

COPY . ./

RUN ls -a

RUN apk add --no-cache make g++ python \
  && npm install -g @nestjs/cli &&  npm install --production

RUN npm run build

FROM node:14.9-alpine

WORKDIR /usr/share/api-gateway

COPY --from=build /usr/share/api-gateway/node_modules ./node_modules
COPY --from=build /usr/share/api-gateway/dist ./dist

EXPOSE 3000

CMD ["node", "dist/main.js"]
