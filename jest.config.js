module.exports = {
  setupFilesAfterEnv: ['jest-extended'],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90
    }
  },
  moduleFileExtensions: ['js', 'json', 'ts'],
  collectCoverage: true,
  collectCoverageFrom: ['**/*.(t|j)s'],
  coverageReporters: ['json', 'text', 'clover', 'html'],
  coverageDirectory: '../coverage',
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest'
  },
  testEnvironment: 'node'
};
