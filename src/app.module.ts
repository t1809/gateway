import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphQLModule, GqlModuleOptions } from '@nestjs/graphql';
import { LoggerModule, PinoLogger } from 'nestjs-pino';

import { TabsQueries } from './graphql/playground/tabs';
// eslint-disable-next-line import/named
import { errorName, formatError } from './common/functions';
import { TariffModule } from './tariff/tariff.module';
import { TariffConfModule } from './tariff-conf/tariff-conf.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        pinoHttp: {
          safe: true,
          prettyPrint: configService.get<string>('NODE_ENV') !== 'production'
        }
      }),
      inject: [ConfigService]
    }),
    GraphQLModule.forRootAsync({
      imports: [ConfigModule, LoggerModule],
      useFactory: async (logger: PinoLogger, config: ConfigService): Promise<GqlModuleOptions> => ({
        logger,
        debug: config.get('NODE_ENV') !== 'production',
        cors: false,
        autoSchemaFile: true,
        sortSchema: true,
        installSubscriptionHandlers: true,
        introspection: true,
        playground: {
          endpoint: '/graphql',
          subscriptionEndpoint: '/sub',
          settings: {
            'request.credentials': 'include'
          },
          tabs: TabsQueries
        },
        context: ({ req, res }): any => ({ req, res, errorName }),
        formatError: (err) => formatError.getError(err)
      }),
      inject: [PinoLogger, ConfigService]
    }),
    TariffModule,
    TariffConfModule
    // AuthModule,
  ]
})
export class AppModule {}
