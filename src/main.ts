import { NestFactory } from '@nestjs/core';
import { NestExpressApplication, ExpressAdapter } from '@nestjs/platform-express';
import { json, urlencoded } from 'express';
import { Logger } from 'nestjs-pino';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import csurf from 'csurf';
import { HttpExceptionFilter } from './common/filters/http-exception.filter';

async function main() {
  const app: NestExpressApplication = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter()
  );
  const configService: ConfigService = app.get(ConfigService);

  app.use(
    cors({
      origin: ['http://localhost:3000', 'http://localhost:3500'],
      credentials: true
    })
  );
  // app.use(csurf({ cookie: true }));
  app.use(cookieParser());
  app.use(json({ limit: '10mb' }));
  app.use(urlencoded({ limit: '10mb', extended: true }));

  // app.useGlobalFilters(new HttpExceptionFilter());

  app.useLogger(app.get(Logger));

  return app.listenAsync(configService.get<number>('GRAPHQL_PORT'));
}

main();
