import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Inject, OnModuleInit } from '@nestjs/common';
import { ClientGrpcProxy } from '@nestjs/microservices';
import { PinoLogger } from 'nestjs-pino';
import { TariffConf } from './tariff-conf.schema';
import { ITariffService } from '../common/services/tariff.service';
import { AllArgs } from '../common/args/general.args';
import { Response } from '../common/types/general.types';
import { NewTariffConfArgs } from './dto/tariff-conf.args';
import { DevelopResponse } from '../common/enums';

@Resolver(() => TariffConf)
export class TariffConfResolver implements OnModuleInit {
  private tariffService: ITariffService;

  constructor(
    @Inject('TariffService')
    private readonly tariffServiceClient: ClientGrpcProxy,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(TariffConfResolver.name);
  }

  onModuleInit(): void {
    this.tariffService = this.tariffServiceClient.getService<ITariffService>('TariffService');
  }

  @Query(() => [TariffConf])
  async tariffsConf(@Args() args: AllArgs): Promise<TariffConf[]> {
    const { tariffsConf } = await this.tariffService.tariffsConf(args).toPromise();
    return Promise.resolve(tariffsConf);
  }

  @Mutation(() => Response)
  async createTariffConf(@Args() args: NewTariffConfArgs): Promise<Response> {
    const resp = await this.tariffService.createConf(args).toPromise();
    // @ts-ignore
    return Promise.resolve({ ...resp, developerCode: DevelopResponse[resp.developerCode] });
  }
}
