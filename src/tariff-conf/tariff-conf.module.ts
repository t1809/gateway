import { Module } from '@nestjs/common';
import { TariffModule } from '../tariff/tariff.module';
import { TariffConfResolver } from './tariff-conf.resolver';

@Module({
  imports: [TariffModule],
  providers: [TariffConfResolver]
})
export class TariffConfModule {}
