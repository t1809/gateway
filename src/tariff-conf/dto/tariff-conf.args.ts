import { ArgsType, Field, Int } from '@nestjs/graphql';
import { ServicesInput } from './tariff-conf.inputs';

@ArgsType()
export class NewTariffConfArgs {
  @Field(() => Int, {
    description: 'Tariff code to which you want to add or modify a tariff conf.'
  })
  code: number;
  @Field({
    description: 'Description or name with which you want to create or modify the tariff conf.'
  })
  description: string;

  @Field(() => Int, {
    description: 'Net value of the service.'
  })
  netWorth: number;

  @Field(() => Int, {
    description: 'Net value of the tariff.'
  })
  netValueTariff: number;

  @Field(() => Int, {
    description: 'Total value of the service.'
  })
  totalValue: number;

  @Field({
    description: 'Effective date of the tariff configuration.'
  })
  startValidity: string;

  @Field(() => [ServicesInput], {
    description: 'Services to be configured from the tariff.'
  })
  services: ServicesInput[];
}
