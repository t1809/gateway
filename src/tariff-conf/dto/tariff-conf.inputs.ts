import { Field, ID, InputType, Int } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';

@InputType()
export class ServicesInput {
  @Field(() => ID, {
    description: 'Identification of the service by database.'
  })
  id: ObjectId;

  @Field({
    description: 'Service code.'
  })
  code: number;

  @Field({
    description: 'Description or name of the service.'
  })
  description: string;

  @Field(() => Int, {
    description: 'Net value of the service.'
  })
  netWorth: number;

  @Field(() => Int, {
    description: 'Net value of the tariff.'
  })
  netValueTariff: number;

  @Field(() => Int, {
    description: 'Total value of the service.'
  })
  totalValue: number;
}
