import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';

@ObjectType()
export class Services {
  @Field(() => ID, {
    nullable: true,
    description: 'Identification of the service by database.'
  })
  id: ObjectId;

  @Field({
    description: 'Service code.'
  })
  code: number;

  @Field({
    description: 'Description or name of the service.'
  })
  description: string;

  @Field(() => Int, {
    description: 'Net value of the service.'
  })
  netWorth: number;

  @Field(() => Int, {
    description: 'Net value of the tariff.'
  })
  netValueTariff: number;

  @Field(() => Int, {
    description: 'Total value of the service.'
  })
  totalValue: number;
}
