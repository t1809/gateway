import { Field, Int, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Node } from '../common/interfaces/node.interface';
import { Services } from './dto/tariff-conf.types';

@ObjectType({
  implements: () => [Node]
})
export class TariffConf implements Node {
  _id: ObjectId;

  @Field(() => Int, {
    description: 'Tariff configuration identification number.'
  })
  code: number;

  @Field({
    description: 'Description or name of the tariff configuration.'
  })
  description: string;

  @Field(() => Boolean, {
    description: 'Status that indicates whether or not this configuration is active.'
  })
  status: boolean;

  @Field({
    description: 'Effective start date of the configuration.'
  })
  startValidity: string;

  @Field({
    nullable: true,
    description: 'Configuration expiration date.'
  })
  termValidity: string;

  @Field(() => [Services], {
    description: 'Tariff configured services.'
  })
  services: Services[];

  createdAt: string;

  updatedAt: string;
}
