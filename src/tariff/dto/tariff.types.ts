import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class TariffType {
  @Field({
    description: 'Name of the type of tariff.'
  })
  name: string;
  @Field({
    description: 'Tariff type code.'
  })
  code: number;
}

@ObjectType()
export class FinancingCode {
  @Field({
    description: 'Funder code.'
  })
  code: number;
  @Field({
    description: 'Gloss or funder name.'
  })
  gloss: string;
}

@ObjectType()
export class Validity {
  @Field({
    description: 'Effective start date.'
  })
  start: string;
  @Field({
    description: 'End date of the validity.'
  })
  end: string;
  @Field({
    description: 'Time zone number.'
  })
  timezoneOffset: number;
}
