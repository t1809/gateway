import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ValidityInput {
  @Field({
    description: 'Effective start date.',
  })
  start: string;

  @Field({
    description: 'End date of the validity.',
  })
  end: string;
}
