import { ArgsType, Field, ID, Int } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { ValidityInput } from './tariff.inputs';

@ArgsType()
export class NewTariffArgs {
  @Field({
    description:
      'Description or name with which you want to create the tariff.',
  })
  description: string;
}

@ArgsType()
export class TariffValidityArgs {
  @Field(() => Int, {
    description: 'Tariff code to which you want to add a validity.',
  })
  code: number;

  @Field(() => ValidityInput, {
    description: 'Datos de la nueva vigencia a ingresar.',
  })
  validity: ValidityInput;
}
