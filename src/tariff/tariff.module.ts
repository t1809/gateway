import { join } from 'path';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientGrpcProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { TariffResolver } from './tariff.resolver';

@Module({
  providers: [
    TariffResolver,
    {
      provide: 'TariffService',
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: configService.get<string>('TARIFF_SVC_URL'),
            package: 'tariff',
            protoPath: join(__dirname, '../_proto/tariff.proto'),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true
            }
          }
        });
      },
      inject: [ConfigService]
    }
  ],
  exports: ['TariffService']
})
export class TariffModule {}
