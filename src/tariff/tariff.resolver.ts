import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Inject, OnModuleInit } from '@nestjs/common';
import { ClientGrpcProxy } from '@nestjs/microservices';
import { PinoLogger } from 'nestjs-pino';
import { Tariff } from './tariff.schema';
import { ITariffService } from '../common/services/tariff.service';
import { AllArgs } from '../common/args/general.args';
import { Response } from '../common/types/general.types';
import { NewTariffArgs, TariffValidityArgs } from './dto/tariff.args';
import { DevelopResponse } from '../common/enums';

@Resolver(() => Tariff)
export class TariffResolver implements OnModuleInit {
  private tariffService: ITariffService;

  constructor(
    @Inject('TariffService')
    private readonly tariffServiceClient: ClientGrpcProxy,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(TariffResolver.name);
  }

  onModuleInit(): void {
    this.tariffService = this.tariffServiceClient.getService<ITariffService>('TariffService');
  }

  @Query(() => [Tariff])
  async tariffs(@Args() args: AllArgs): Promise<Tariff[]> {
    const { tariffs } = await this.tariffService.tariffs(args).toPromise();
    return Promise.resolve(tariffs);
  }

  @Mutation(() => Response)
  async addTariff(@Args() args: NewTariffArgs): Promise<Response> {
    const resp = await this.tariffService.create(args).toPromise();
    // @ts-ignore
    return Promise.resolve({ ...resp, developerCode: DevelopResponse[resp.developerCode] });
  }

  @Mutation(() => Response)
  async addTariffValidity(@Args() args: TariffValidityArgs): Promise<Response> {
    const resp = await this.tariffService.createValidity(args).toPromise();
    // @ts-ignore
    return Promise.resolve({ ...resp, developerCode: DevelopResponse[resp.developerCode] });
  }
}
