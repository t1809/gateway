import { Field, Int, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Node } from '../common/interfaces/node.interface';
import { FinancingCode, TariffType, Validity } from './dto/tariff.types';

@ObjectType({
  implements: () => [Node]
})
export class Tariff implements Node {
  _id: ObjectId;

  @Field(() => Int, {
    description: 'Tariff identification number.'
  })
  code: number;

  @Field({
    description: 'Description or name of the tariff.'
  })
  description: string;

  @Field(() => Int, {
    description: 'Number of the holding company to which the tariff belongs.'
  })
  holding: number;

  @Field(() => TariffType, {
    nullable: true,
    description: 'Definition of the type of tariff.'
  })
  tariffType: TariffType;

  @Field(() => FinancingCode, {
    description: 'Data of the funder of the fee.'
  })
  financingCode: FinancingCode;

  @Field(() => Int, {
    description: 'Reference tariff number.'
  })
  tariffRef: number;

  @Field(() => [Validity], {
    description: 'Validity recorded for the tariff.'
  })
  validity: Validity[];

  createdAt: string;

  updatedAt: string;

  constructor(partial: Partial<Tariff>) {
    Object.assign(this, partial);
  }
}
