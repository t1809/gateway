import { BusinessQueries, BusinessVariables } from './queries/business';
import { TariffQueries, TariffVariables } from './queries/tariff';
import { TariffConfQueries, TariffConfVariables } from './queries/tariff-conf';

export const TabsQueries = [
  {
    name: 'Tariff Queries',
    endpoint: '/graphql',
    query: TariffQueries,
    variables: TariffVariables
  },
  {
    name: 'Tariff Conf Queries',
    endpoint: '/graphql',
    query: TariffConfQueries,
    variables: TariffConfVariables
  }
  // {
  //   name: 'Business Queries',
  //   endpoint: '/graphql',
  //   query: BusinessQueries,
  //   variables: BusinessVariables
  // }
];
