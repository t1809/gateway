export const TariffQueries = `
query Tariffs {
  tariffs {
    id
    code
    description
    holding
    tariffType {
      code
      name
    }
    financingCode {
      code
      gloss
    }
    validity {
      start
      end
      timezoneOffset
    }
    tariffRef
    createdAt
    updatedAt
  }
}

mutation AddTariff($description: String!) {
  addTariff(description: $description) {
    message
    developerCode
  }
}

mutation AddTariffValidity($code: Int!, $validity: ValidityInput!) {
  addTariffValidity(code: $code, validity: $validity) {
    message
    developerCode
  }
}
`;

const Variables = {
  description: 'Segundo Arancels',
  code: 2,
  validity: {
    start: 'Wed Aug 25 2021 12:30:51 GMT-0500 (Colombia Standard Time)',
    end: 'Wed Aug 25 2021 12:30:51 GMT-0500 (Colombia Standard Time)'
  }
};

export const TariffVariables = JSON.stringify(Variables);
