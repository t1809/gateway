export const TariffConfQueries = `
query TariffsConf {
  tariffsConf {
    id
    code
    description
    status
    startValidity
    termValidity
    services {
      id
      code
      description
      netWorth
      netValueTariff
      totalValue
    }
    createdAt
    updatedAt
  }
}

mutation AddTariffConf($code: Int!, $description: String!, $netValueTariff: Int!, $netWorth: Int!, $totalValue: Int!, $startValidity: String!, $services: [ServicesInput!]!) {
  createTariffConf(code: $code, description: $description, netWorth: $netWorth, netValueTariff: $netValueTariff, totalValue: $totalValue, startValidity: $startValidity, services: $services) {
    message
    developerCode
  }
}
`;

const Variables = {
  code: 2,
  description: 'Segundo Arancels',
  netWorth: 1500,
  netValueTariff: 1500,
  totalValue: 1500,
  startValidity: 'Wed Aug 25 2021 12:30:51 GMT-0500 (Colombia Standard Time)',
  services: [
    {
      id: '61267e4b89e9e6daaa6d9394',
      code: 36,
      description: 'Prestacion odontologica',
      netWorth: 1500,
      netValueTariff: 1500,
      totalValue: 1500
    }
  ]
};

export const TariffConfVariables = JSON.stringify(Variables);
