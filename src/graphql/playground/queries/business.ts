export const BusinessQueries = `
query BusinessSectors {
  businessSectors {
    id
    description
    id
    createdAt
    updatedAt
  }
}

query BusinessStatistics {
  businessStatistics {
    totalCompanies
  }
}

query Business {
  business {
    ...BusinessData
  }
}

query Company($id: ID!) {
  company(id: $id) {
    ... on Business {
      ...BusinessData
    }
    ... on Response {
      message
      developerCode
    }
  }
}

mutation Create($data: BusinessCreateData!) {
  addBusiness(data: $data) {
    message
    developerCode
  }
}

mutation Update($id: ID!, $update: BusinessUpdateData!) {
  updateBusiness(id: $id, data: $update) {
    message
    developerCode
  }
}

mutation UpdateBusinessMultimedia($id: ID!, $updateMultimedia: BusinessMultimediaUpdate!) {
  updateMultimedia(id: $id, data: $updateMultimedia) {
    message
    developerCode
  }
}

fragment BusinessData on Business {
  id
    name
    businessName
    idType {
      id
      name
      shortName
      isActive
      createdAt
      updatedAt
    }
    idNumber
    city
    country
    rating
    multimedia {
      id
      logo
      images
      video
      createdAt
      updatedAt
    }
    businessSector {
      id
      description
      icon
      createdAt
      updatedAt
    }
    status
    constitutionAt
    createdAt
    updatedAt
}
`;

const Variables = {
  id: '5f8759d841e5c8bb5c2724cc',
  updateMultimedia: {
    video: 'https://yotube.com/'
  },
  update: {
    name: 'Tigo Une AC'
  },
  data: {
    name: 'Company Demo',
    businessName: 'S.A.S',
    idType: '5f8769b921df0d0c3d1adc60',
    idNumber: '986532615',
    country: 'CO',
    constitutionAt: new Date()
  }
};

export const BusinessVariables = JSON.stringify(Variables);
