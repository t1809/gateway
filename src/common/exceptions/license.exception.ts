import { HttpException, HttpStatus } from '@nestjs/common';
import { DevelopResponse } from '../enums';

export const LicenseExceptionDefinition = {
  name: 'INVALID_LICENSE',
  message: 'Your license has expired or you do not have a valid license.',
  statusCode: 403
};

export class LicenseExpiredException extends HttpException {
  developerCode: DevelopResponse;

  constructor() {
    super('Forbidden', HttpStatus.FORBIDDEN);
    this.message = LicenseExceptionDefinition.message;
    this.developerCode = DevelopResponse.LICENSE_EXPIRED_OR_INVALID;
    throw new Error(LicenseExceptionDefinition.name);
  }
}
