import { HttpException, HttpStatus } from '@nestjs/common';
import { DevelopResponse } from '../enums';

export const PermissionsExceptionDefinition = {
  name: 'INSUFFICIENT_PERMISSIONS',
  message: 'You do not have sufficient permissions to execute this action.',
  statusCode: 403
};

export class PermissionsException extends HttpException {
  developerCode: DevelopResponse;

  constructor() {
    super('Forbidden', HttpStatus.FORBIDDEN);
    this.message = PermissionsExceptionDefinition.message;
    this.developerCode = DevelopResponse.WITHOUT_ENOUGH_PERMISSIONS;
    throw new Error(PermissionsExceptionDefinition.name);
  }
}
