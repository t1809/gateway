import { Field, ID, InputType, Int } from '@nestjs/graphql';
import { Max, Min } from 'class-validator';

@InputType()
export class AllInput {
  @Field(() => Int, {
    description: 'Enter how many elements you want to skip in the query.'
  })
  @Min(0)
  skip = 0;

  @Field(() => Int, {
    description: 'Indicates how many elements you want to take from the query.'
  })
  @Min(0)
  @Max(15)
  take = 0;
}
