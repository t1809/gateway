import { Field, ObjectType } from '@nestjs/graphql';
import { IsEnum } from 'class-validator';
import { DevelopResponse } from '../enums';

@ObjectType()
export class Response {
  @Field({
    description: 'Message body of the response of a query.'
  })
  message: string;

  @IsEnum(DevelopResponse)
  @Field(() => DevelopResponse)
  developerCode: DevelopResponse;

  constructor(partial: Partial<Response>) {
    Object.assign(this, partial);
  }
}
