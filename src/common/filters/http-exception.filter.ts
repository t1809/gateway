import { ArgumentsHost, Catch, HttpException } from '@nestjs/common';
import { GqlArgumentsHost, GqlExceptionFilter } from '@nestjs/graphql';
import { Response } from '../types/general.types';
import { DevelopResponse } from '../enums';

@Catch(HttpException)
export class HttpExceptionFilter implements GqlExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost): any {
    const gqlHost = GqlArgumentsHost.create(host);
    const statusCode = exception.getStatus();

    if (statusCode === 401)
      return new Response({
        message: 'You are not authenticated',
        developerCode: DevelopResponse.UNAUTHORIZED_EXCEPTION
      });

    if (statusCode === 403) {
      return new Response({
        message: exception.message,
        // @ts-ignore
        developerCode: exception.developerCode
      });
    }
  }
}
