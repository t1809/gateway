import { Observable } from 'rxjs';
import { Metadata } from 'grpc';
import { AllArgs } from '../args/general.args';
import { Response } from '../types/general.types';
import { Tariff } from '../../tariff/tariff.schema';
import { NewTariffArgs, TariffValidityArgs } from '../../tariff/dto/tariff.args';
import { NewTariffConfArgs } from '../../tariff-conf/dto/tariff-conf.args';

export interface ITariffService {
  tariffs(data: AllArgs, metadata?: Metadata): Observable<{ tariffs: Tariff[] }>;
  create(data: NewTariffArgs, metadata?: Metadata): Observable<Response>;
  createValidity(data: TariffValidityArgs, metadata?: Metadata): Observable<Response>;
  tariffsConf(data: AllArgs, metadata?: Metadata): Observable<{ tariffsConf: any[] }>;
  createConf(data: NewTariffConfArgs, metadata?: Metadata): Observable<Response>;
}
