import { Module } from '@nestjs/common';
import { DateScalar } from './scalars/date.scalar';
import { PasswordUtils } from './functions';
import { ComplexityPlugin } from './plugins/ComplexityPlugin';

@Module({
  providers: [DateScalar, PasswordUtils],
  exports: [PasswordUtils]
})
export class CommonModule {}
